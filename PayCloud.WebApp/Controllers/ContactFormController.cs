﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayCloud.Services.Providers;
using PayCloud.WebApp.Models;

namespace PayCloud.WebApp.Controllers
{
    public class ContactFormController : Controller
    {
        private readonly IDateTimeNowProvider dateTimeNowProvider;
        private readonly IEmailSender emailSender;
        private readonly IFileServicesProvider fileServicesProvider;

        public ContactFormController(IDateTimeNowProvider dateTimeNowProvider,
            IEmailSender emailSender,
            IFileServicesProvider fileServicesProvider)
        {
            this.dateTimeNowProvider = dateTimeNowProvider ?? throw new ArgumentNullException(nameof(dateTimeNowProvider));
            this.emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
            this.fileServicesProvider = fileServicesProvider ?? throw new ArgumentNullException(nameof(fileServicesProvider));
        }

        // GET: ContactForm/GetAboutAppPartial
        public ActionResult GetAboutAppPartial()
        {
            return this.PartialView("_About");
        }

        // GET: ContactForm/SendUserMessage
        public ActionResult GetUserMessagePartialView()
        {
            return this.PartialView("_SendUserMessagePartial");
        }

        // POST: ContactForm/SendUserMessage
        [HttpPost]
        public ActionResult SendUserMessage(ContactFormDto contactFormDto)
        {
            string fileContent;
            const string filePath = "UserMessagesLog.txt";
            try
            {
                // TODO: Send and email
                if (string.IsNullOrWhiteSpace(contactFormDto?.Title)
                    && string.IsNullOrWhiteSpace(contactFormDto?.Message)
                    && string.IsNullOrWhiteSpace(contactFormDto?.Name)
                    && string.IsNullOrWhiteSpace(contactFormDto?.Email))
                {
                    return this.Json("Message not sent, because there wasn't any message.");
                }
                else
                {
                    fileContent = $"Time: {this.dateTimeNowProvider.Now.ToString()}\nName: {contactFormDto?.Name?.Trim()}\nEmail: {contactFormDto?.Email?.Trim()}\nTitle: {contactFormDto?.Title?.Trim()}\nMessage: {contactFormDto?.Message?.Trim()}";
                    this.fileServicesProvider.WriteToTextFile(filePath, fileContent);
                }
            }
            catch
            {
                return this.Json("Message not sent, because the system is busy. Please, try again later.");
            }
            try
            {
                string fromEmail = "cloud.team.public.relations@gmail.com";
                string ccEmail = "";
                string bccEmail = "cloud.team.public.relations@gmail.com";
                this.emailSender.SendMail(
                    contactFormDto.Email,
                    fromEmail,
                    ccEmail,
                    bccEmail,
                    $"New pay cloud user's message {this.dateTimeNowProvider.Now.ToString()}",
                    fileContent);
                this.fileServicesProvider.WriteToTextFile(filePath, "\n\n");
                return this.Json("");
            }
            catch (Exception)
            {
                this.fileServicesProvider.WriteToTextFile(filePath, "ERROR: Email not sent!\n\n");
                return this.Json("");
            }
        }
    }
}
