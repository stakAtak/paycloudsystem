﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PayCloud.Services.Providers
{
    public class FileServicesProvider : IFileServicesProvider
    {
        public bool FileExists(string filePath)
        {
            return System.IO.File.Exists(filePath.Trim());
        }

        public (bool result, string message) DeleteFile(string filePath)
        {
            // Delete a file by using File class static method...
            if (FileExists(filePath.Trim()))
            {
                // Use a try block to catch IOExceptions, to
                // handle the case of the file already being
                // opened by another process.
                try
                {
                    System.IO.File.Delete(filePath);
                    return (true, "");
                }
                catch (System.IO.IOException e)
                {
                    return (false, e.Message);
                }
            }
            return (true, $"Missing file: {filePath}");
        }

        public (bool result, string message) WriteToTextFile(string filePath, string fileContent)
        {
            string UploadsFolder = System.IO.Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot\\images\\BannersStorage");

            string path = Path.Combine(UploadsFolder, filePath);
            CreateFolder(UploadsFolder);

            try
            {
                // Example #4: Append new text to an existing file.
                // The using statement automatically flushes AND CLOSES the stream and calls 
                // IDisposable.Dispose on the stream object.
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
                {
                    file.WriteLine(fileContent);
                }
            }
            catch (Exception)
            {
                return (result: false, message: path);
            }
            return (result: true, message: path);
        }

        public (bool result, string message) ReadFromTextFile(string filePath, string fileContent)
        {
            string UploadsFolder = System.IO.Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot\\images\\BannersStorage");

            string path = Path.Combine(UploadsFolder, filePath);

            StringBuilder sb = new StringBuilder();
            try
            {
                // Open the file to read from.
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        sb.AppendLine(s);
                    }
                }
            }
            catch (Exception)
            {
                return (result: false, message: path);
            }
            return (result: true, message: sb.ToString());
        }

        public (bool result, string message) CreateFolder(string filePath)
        {
            // Create folder...
            if (FileExists(filePath.Trim()))
            {
                return (true, $"Folder already exists: {filePath}");
            }
            try
            {
                System.IO.Directory.CreateDirectory(filePath);
                return (true, "");
            }
            catch (System.IO.IOException e)
            {
                return (false, e.Message);
            }
        }
    }
}
