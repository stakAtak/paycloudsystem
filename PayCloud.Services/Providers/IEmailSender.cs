﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayCloud.Services.Providers
{
    public interface IEmailSender
    {
        string SendMail(
            string toList,
            string from,
            string ccList,
            string bccList,
            string subject,
            string body);
    }
}
