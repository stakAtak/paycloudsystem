﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace PayCloud.Services.Providers
{
    public class EmailSender : IEmailSender
    {
        //public Task SendEmailAsync(string email, string subject, string message)
        //{
        //    // https://stackoverflow.com/questions/704636/sending-email-through-gmail-smtp-server-with-c-sharp
        //    var client = new SmtpClient("smtp.gmail.com", 587) {
        //        Credentials = new NetworkCredential("myusername@gmail.com", "mypwd"),
        //        EnableSsl = true
        //    };
        //    client.Send("myusername@gmail.com", "myusername@gmail.com", "test", "testbody");
        //    //outputProvider.WriteLine("Sent");
        //    //outputProvider.ReadLine();
        //    return Task.CompletedTask;
        //}

        public string SendMail(
            string toList,
            string from,
            string ccList,
            string bccList,
            string subject,
            string body)
        {
            const string siteAdminEmail = "cloud.team.public.relations@gmail.com";
            const string passwordEmail = "vny@ppLavch0";
            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string msg = string.Empty;
            try
            {
                message.From = new MailAddress(from);
                try
                {
                    message.To.Add(toList);
                }
                catch (Exception)
                {
                    toList = siteAdminEmail;
                    message.To.Add(toList);
                }
                char[] separator = { ',', ';', ' ' };
                if (!string.IsNullOrWhiteSpace(ccList))
                {
                    var ccListTemp = ccList.Split(separator, StringSplitOptions.RemoveEmptyEntries).ToList().Distinct();
                    foreach (var item in ccListTemp)
                    {
                        try
                        {
                            message.CC.Add(item);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                if (!string.IsNullOrWhiteSpace(bccList))
                {
                    var bccListTemp = bccList.Split(separator, StringSplitOptions.RemoveEmptyEntries).ToList().Distinct();
                    foreach (var item in bccListTemp)
                    {
                        try
                        {
                            message.Bcc.Add(item);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                message.Subject = subject;
                message.IsBodyHtml = false;
                message.Body = body;
                // We use gmail as our smtp client
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential(siteAdminEmail, passwordEmail);

                smtpClient.Send(message);
                msg = "Successful<BR>";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                throw;
            }
            return msg;
        }
    }
}
