﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayCloud.Services.Providers
{
    public interface IFileServicesProvider
    {
        bool FileExists(string filePath);
        (bool result, string message) DeleteFile(string filePath);
        (bool result, string message) CreateFolder(string filePath);
        (bool result, string message) WriteToTextFile(string filePath, string fileContent);
        (bool result, string message) ReadFromTextFile(string filePath, string fileContent);
    }
}
